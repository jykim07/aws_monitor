package monitor;

import configuration.*;
import java.util.HashMap;
import java.util.Map;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Monitor {
	static String dbIP = null;
	static int dbPort;
	static String dbName = null;
	static String graphCollName = null;
	static String metricCollName = null;
	static String historyCollName = null;

	public static void main(String[] args) {
	
		// Read and assign configuration parameters from the configuration file 
		Configuration.setConfiguration();		
		dbIP = Configuration.getDBIP();
		dbPort = Configuration.getDBPort();
		dbName = Configuration.getDBName();
		graphCollName = Configuration.getGraphCollection();
		metricCollName = Configuration.getMetricCollection();
		historyCollName = Configuration.getHistoryCollection();
		
		try
		{
			// open a socket for communication with clients
			// initialize communication classes using the socket
			DatagramSocket socket = new DatagramSocket(Configuration.getSocketport());
			Socketserver server = new Socketserver(socket);
			
			// initialize the collection
			setGraphCollection graphColl = new setGraphCollection(dbIP, dbPort, dbName, graphCollName);
			setMetricCollection metricColl = new setMetricCollection(dbIP, dbPort, dbName, metricCollName);
			setHistoryCollection historyColl = new setHistoryCollection(dbIP, dbPort, dbName, historyCollName);
		
			// start receiving data from clients
			server.start();		
			
		} catch (SocketException e) {
			e.printStackTrace();
		}		
	}
}
