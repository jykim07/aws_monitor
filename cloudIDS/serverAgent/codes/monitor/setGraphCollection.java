package monitor;

import configuration.*;
import datatype.*;
import com.mongodb.*;
import java.util.*;
import java.text.DecimalFormat;

public class setGraphCollection
{		
	static Mongo mongo;
	static DB db;	
	static DBCollection collection;
	
	static ArrayList<String> dataTypes = new ArrayList<String>();
	static long oplogQueryTS = 0;  // 1000000000, 1430419645
	static long interval = Configuration.getInterval();
	
	// Configure the history DB according to the configuration file
	public setGraphCollection(String dbIP, int dbPort, String dbName, String collName) throws MongoException 
	{	
		try {
				mongo = new Mongo(dbIP, dbPort);
				db = mongo.getDB(dbName);
				collection = db.getCollection(collName);
			
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public setGraphCollection(){}
/*
	public static long getNextQueryTS()
	{
		oplogQueryTS = System.currentTimeMillis() / 1000L;
		
		Date nextQueryTS = new Date((oplogQueryTS-interval)*1000L); 		
		long queryNextTS = (long)nextQueryTS.getTime()/1000;

		return queryNextTS;
	}		
*/	
    //db.graph.findOne({"IP":"172.31.2.109","MIN.Metric.details":{$exists:true},"MIN.Metric.details.TS":{$gte:"1497036498"}})
	//	db.graph.findOne({"IP":"172.31.2.109","MIN.RQ.neighbor":{$exists:true},"MIN.RQ.neighbor.TS":{$gte:"1497036498"}})
	public static boolean checkIfDBdrop(String ip)
	{		
			String type = "MIN.Metric.details";

			BasicDBObject query = new BasicDBObject();
			query.put("IP",ip);
			query.put(type, new BasicDBObject("$exists",true));
			query.put(type+".TS",new BasicDBObject("$lte", Long.toString(System.currentTimeMillis() / 1000L-10)));
			
			DBObject obj = collection.findOne(query);		
	
			if(obj!=null)	
				return true;
			
			else
				return false;
//		}
	}

	// All changes are updated into MINUTE fields 
	public static void updateMetric(String type, MetricInfo request)
	{	
		String ip = request.ip;
		String exIP = request.exIP;
		
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		
		BasicDBObject metric = new BasicDBObject("details", new BasicDBObject("TS", Long.toString(System.currentTimeMillis()/1000))
																.append("name", request.name+ "("+ request.option+ ")")
																.append("avg", Double.toString(request.avg))
																.append("min", Double.toString(request.min))
																.append("max", Double.toString(request.max))
																.append("unit", request.unit));
																
		BasicDBObject newValue = new BasicDBObject(type, metric);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));		
		
		if(checkIfDBdrop(ip))
		{
			BasicDBObject document = new BasicDBObject();
			collection.remove(document);								
		}
	
		collection.update(queryIP, updateValue,true,false);				

		BasicDBObject updateExIP = new BasicDBObject("$set", new BasicDBObject("exIP", exIP));			
		collection.update(queryIP, updateExIP, false, false);			
	}

	// All changes are updated into MINUTE fields 
	public static void updateUtilization(String ip, String exIP, String type, String value)
	{					
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", (int)(System.currentTimeMillis()/1000))
									.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));
						
		collection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));			
		collection.update(queryIP, updateLastValue, false, false); 
	}
	
	// All changes are updated into MINUTE fields 
	public static void updateRequest(String type, RequestInfo request)
	{	
		String ip = request.myIP;
		String exIP = request.exIP;
		
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject neighbor = new BasicDBObject("neighbor", new BasicDBObject("TS", Long.toString(System.currentTimeMillis()/1000))
																.append("address", request.neighIP)
																.append("port", Integer.toString(request.port))
																.append("req", Integer.toString(request.req))
																.append("res", Integer.toString(request.res))
																.append("ratio", Double.toString(request.ratio)));
		
		BasicDBObject newValue = new BasicDBObject(type, neighbor);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));

		collection.update(queryIP, updateValue,true,false);			

		BasicDBObject updateExIP = new BasicDBObject("$set", new BasicDBObject("exIP", exIP));			
		collection.update(queryIP, updateExIP, false, false);				
	}
}
