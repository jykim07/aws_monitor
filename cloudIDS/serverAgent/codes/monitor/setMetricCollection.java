package monitor;

import datatype.*;
import com.mongodb.*;
import java.util.*;
import java.text.DecimalFormat;

public class setMetricCollection
{		
	static Mongo mongo;
	static DB db;	
	static DBCollection collection;
	
	static ArrayList<String> dataTypes = new ArrayList<String>();
	
	// Configure the history DB according to the configuration file
	public setMetricCollection(String dbIP, int dbPort, String dbName, String collName) throws MongoException 
	{	
		try {
				mongo = new Mongo(dbIP, dbPort);
				db = mongo.getDB(dbName);
				collection = db.getCollection(collName);
			
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public setMetricCollection(){}

	// All changes are updated into MINUTE fields 
	public static void updateCurrentMetric(MetricInfo request)
	{	
		String ip = request.ip;
		
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		String name = request.name;
		name = name.replace('.','_');

		String metric = Double.toString(request.max);
		if(!request.unit.equals("none"))
			metric += " / " +request.unit;
		
		BasicDBObject value = new BasicDBObject(name+ "("+ request.option+ ")", metric);
		BasicDBObject updateLastValue = new BasicDBObject("$set", value);	
				
		collection.update(queryIP, updateLastValue, true, false); 			
	}
}
