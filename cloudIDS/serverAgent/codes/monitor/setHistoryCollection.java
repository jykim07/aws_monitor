package monitor;

import datatype.*;
import com.mongodb.*;
import java.util.*;
import java.text.DecimalFormat;

public class setHistoryCollection
{		
	static Mongo mongo;
	static DB db;	
	static DBCollection collection;
	
	static ArrayList<String> dataTypes = new ArrayList<String>();
	
	// Configure the history DB according to the configuration file
	public setHistoryCollection(String dbIP, int dbPort, String dbName, String collName) throws MongoException 
	{	
		try {
				mongo = new Mongo(dbIP, dbPort);
				db = mongo.getDB(dbName);
				collection = db.getCollection(collName);
			
		} catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}
	}
	
	public setHistoryCollection(){}

	// All changes are updated into MINUTE fields 
	public static void updateMetric(String type, MetricInfo request)
	{	
		String ip = request.ip;
		String exIP = request.exIP;
		
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		
		BasicDBObject metric = new BasicDBObject("details", new BasicDBObject("TS", Long.toString(System.currentTimeMillis()/1000))
																.append("name", request.name+ "("+ request.option+ ")")
																.append("avg", Double.toString(request.avg))
																.append("min", Double.toString(request.min))
																.append("max", Double.toString(request.max))
																.append("unit", request.unit));
		
		BasicDBObject newValue = new BasicDBObject(type, metric);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));
	
		collection.update(queryIP, updateValue,true,false);				

		BasicDBObject updateExIP = new BasicDBObject("$set", new BasicDBObject("exIP", exIP));			
		collection.update(queryIP, updateExIP, false, false);		
	}
	
	// All changes are updated into MINUTE fields 
	public static void updateUtilization(String ip, String exIP, String type, String value)
	{					
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject newValue = new BasicDBObject("TS", (int)(System.currentTimeMillis()/1000))
									.append(type,value);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));
						
		collection.update(queryIP,updateValue,true,false);
			
		BasicDBObject updateLastValue = new BasicDBObject("$set", new BasicDBObject("L"+type, value));			
		collection.update(queryIP, updateLastValue, false, false); 
	}
	
	// All changes are updated into MINUTE fields 
	public static void updateRequest(String type, RequestInfo request)
	{	
		String ip = request.myIP;
		String exIP = request.exIP;
		
		BasicDBObject queryIP = new BasicDBObject("IP",ip);
		BasicDBObject neighbor = new BasicDBObject("neighbor", new BasicDBObject("TS", Long.toString(System.currentTimeMillis()/1000))
																.append("address", request.neighIP)
																.append("port", Integer.toString(request.port))
																.append("req", Integer.toString(request.req))
																.append("res", Integer.toString(request.res))
																.append("ratio", Double.toString(request.ratio)));
		
		BasicDBObject newValue = new BasicDBObject(type, neighbor);
		BasicDBObject updateValue = new BasicDBObject("$push", new BasicDBObject("MIN",newValue));

		collection.update(queryIP, updateValue,true,false);			

		BasicDBObject updateExIP = new BasicDBObject("$set", new BasicDBObject("exIP", exIP));			
		collection.update(queryIP, updateExIP, false, false);			
	}
}
