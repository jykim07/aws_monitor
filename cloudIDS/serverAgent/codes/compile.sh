#!/bin/bash  

user="ec2-user"
DIR="/home/$user/cloudIDS/serverAgent/codes"

###### compile the program #####


cd $DIR;javac -d $DIR monitor/*.java;javac -d $DIR datatype/*.java;javac -d $DIR configuration/*.java;jar cfm cloudIDSServer.jar manifest.txt datatype/*.class configuration/*.class monitor/*.class;
