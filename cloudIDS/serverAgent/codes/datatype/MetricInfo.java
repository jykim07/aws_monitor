package datatype;

import java.io.Serializable;

public class MetricInfo implements Serializable 
{
    private static final long serialVersionUID = 1L;
	public String ip;
	public String exIP;
	public String name;
	public String unit;
	public String option;
    public double avg;
    public double min;
	public double max;


    public MetricInfo(String ip, String exIP) 
	{
        this.ip = ip;
		this.exIP = exIP;
		this.name = null;
		this.unit = null;
		this.option = null;
		this.unit = null;
		this.avg = 0;
        this.min = 0;
        this.max = 0;
    }
}