package datatype;

import java.io.Serializable;

public class UAInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    public String ip;
	public String exIP;
    public String UA = "normal";
	
    public UAInfo(String ip, String exIP) {
        this.ip = ip;
		this.exIP = exIP;
    }
}