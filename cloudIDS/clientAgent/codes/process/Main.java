package process;

import configuration.Configuration;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import datatype.*;

public class Main {

    RemoteServer rs = new RemoteServer();
	static private String exIP = null;
	static private String pmIP = null;
	static private String NIC = null;

    public static void main(String[] args) throws UnknownHostException, 
            SocketException, IOException, InterruptedException, MalformedURLException 
	{
        
		// Loading configuration options
        Configuration.setConfiguration();
		Configuration.printAll();
		
		try
		{
			// Get the IP address of the physical server hosting this agent
			exIP = getExIP();
			pmIP = getIp();
		} catch (Exception e) {
            UnknownHostException unknownHostException = new UnknownHostException(
                    "Failed to determine LAN address: " + e);
            unknownHostException.initCause(e);
            throw unknownHostException;
        }
		
		
		// Get the NIC 
		NIC = Configuration.getNIC();

        // Initial contact of the agent. Maybe the relay server or the monitoring server May change subsequently.
        RemoteServer.SetIPPort(Configuration.getRemoteserver(),
                Configuration.getSocketPort());

        // s2s handles all of the transmit of packets. Whenever a part of code
        // wants to send packets, s2s utility is called.
        DatagramSocket socket = new DatagramSocket(
                Configuration.getSocketPort());
        Send2Server s2s = new Send2Server(socket);
        s2s.init();

        // exit if IP of the pm is not known.
        if (pmIP == null) {
            System.err.println("Cannot get the IP of this server.");
        }
  
		getMetric metric = new getMetric();
//		getSystem system = new getSystem(); 
//		getNetwork network = new getNetwork();
/*		getRequest request = new getRequest();
		
		if(Configuration.getServicePort() != 0)
			request.startRequest();
*/    }

	public static String getExIP() throws Exception 
	{
		URL whatismyip = new URL("http://checkip.amazonaws.com");
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            String ip = in.readLine();
            return ip;
        } 
		
		finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();

                } catch (Exception e) {
            UnknownHostException unknownHostException = new UnknownHostException(
                    "Failed to determine LAN address: " + e);
            unknownHostException.initCause(e);
            throw unknownHostException;
        }
            }
        }		
	}

    public static String getIp() throws UnknownHostException {
        try {
            InetAddress candidateAddress = null;
			String virtualBrdgIP = Configuration.getVirtualBrdgIP();

            // Iterate all NICs (network interface cards)...
            for (Enumeration<NetworkInterface> ifaces = NetworkInterface
                    .getNetworkInterfaces(); ifaces.hasMoreElements();) {
                NetworkInterface iface = (NetworkInterface) ifaces
                        .nextElement();
                // Iterate all IP addresses assigned to each card...
                for (Enumeration<InetAddress> inetAddrs = iface
                        .getInetAddresses(); inetAddrs.hasMoreElements();) {
                    InetAddress inetAddr = (InetAddress) inetAddrs
                            .nextElement();
                    //System.out.println(inetAddr.toString());
                    if (!inetAddr.isLoopbackAddress()) {

                        if (extractIPv4(inetAddr.toString())) {
                            // Found non-loopback site-local address. Return it
                            // immediately...
                            String resultIP = inetAddr.toString().split("/")[1];
							if(resultIP.equals(virtualBrdgIP))
								continue;

                            return resultIP;
                        } 
						
						else if (candidateAddress == null) {
                            // Found non-loopback address, but not necessarily
                            // site-local.
                            // Store it as a candidate to be returned if
                            // site-local address is not subsequently found...
                            candidateAddress = inetAddr;
                            // Note that we don't repeatedly assign non-loopback
                            // non-site-local addresses as candidates,
                            // only the first. For subsequent iterations,
                            // candidate will be non-null.
                        }
                    }
                }
            }
            if (candidateAddress != null) {
                // We did not find a site-local address, but we found some other
                // non-loopback address.
                // Server might have a non-site-local address assigned to its
                // NIC (or it might be running
                // IPv6 which deprecates the "site-local" concept).
                // Return this non-loopback candidate address...
                return candidateAddress.toString().split("/")[1];
            }
            // At this point, we did not find a non-loopback address.
            // Fall back to returning whatever InetAddress.getLocalHost()
            // returns...
            InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
            if (jdkSuppliedAddress == null) {
                throw new UnknownHostException(
                        "The JDK InetAddress.getLocalHost() method unexpectedly returned null.");
            }
            return jdkSuppliedAddress.toString().split("/")[1];
        } catch (Exception e) {
            UnknownHostException unknownHostException = new UnknownHostException(
                    "Failed to determine LAN address: " + e);
            unknownHostException.initCause(e);
            throw unknownHostException;
        }
    }
    
    public static boolean extractIPv4(String str) {
        String IPV4_PATTERN = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
        Pattern patternIP = Pattern.compile(IPV4_PATTERN);
        Matcher matcher = patternIP.matcher(str);

        return matcher.find();
    }
	
	public static String getSystemIP()
	{
		return pmIP;		
	}
	
	public static String getExternalIP()
	{
		return exIP;		
	}	
	
}
