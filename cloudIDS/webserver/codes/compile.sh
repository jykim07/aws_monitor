#!/bin/bash  

user="ec2-user"
DIR="/home/$user/cloudIDS/webserver/codes"

###### compile the program #####


cd $DIR;javac -d $DIR main/*.java;javac -d $DIR servlet/*.java;javac -d $DIR configuration/*.java;jar cfm cloudIDSWeb.jar manifest.txt main/*.class configuration/*.class servlet/*.class;

###### deploy the latest jar 
user="ec2-user"
tomcatDIR="/usr/share/tomcat"

sudo cp cloudIDSWeb.jar $tomcatDIR/lib

sudo service tomcat restart
