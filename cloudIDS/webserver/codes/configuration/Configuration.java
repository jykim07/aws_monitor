package configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class Configuration {
    private static String dbIP = null;
    private static int dbPort;
    private static long interval;
	private static String dbName=null;
	private static String graphCollName=null;
	private static String historyCollName=null;
	private static String metricCollName=null;
	
	public static void setConfiguration()
	{
		try
		{
			String dir = "/usr/share/tomcat/webapps/ROOT/cloudIDS/Config.txt";
			File readFile = new File(dir);
			BufferedReader inFile = new BufferedReader(new FileReader(readFile));
			String sLine = null;
			
			while((sLine = inFile.readLine()) != null)
			{
				String parameter = sLine.split("=")[0];
				String value = sLine.split("=")[1];

				if(parameter.equals("DB IP address"))
					dbIP = value;
				
				else if(parameter.equals("DB port"))
					dbPort = Integer.parseInt(value);
				
				else if(parameter.equals("DB name"))
					dbName = value;

				else if(parameter.equals("History collection name"))
					historyCollName = value;
				
				else if(parameter.equals("Metric collection name"))
					metricCollName = value;	
				
				else if(parameter.equals("Graph collection name"))
					graphCollName = value;					

				else if(parameter.equals("Interval"))
					interval = Long.parseLong(value);				
					
				sLine = null;
			}
			inFile.close();
		}
		catch(Exception ex)
		{
	    	ex.printStackTrace();
		}
	}	
	
    public static String getDBIP(){
    	return dbIP;
    }
    
    public static int getDBPort(){
    	return dbPort;
    }
	
    public static String getDBName(){
    	return dbName;
    }
	
	public static String getHistoryColl(){
    	return historyCollName;
    }
	
	public static String getMetricColl(){
    	return metricCollName;
    }	
	
	public static String getGraphColl(){
    	return graphCollName;
    }
	
    public static long getInterval(){
    	return interval;
    }	
}
