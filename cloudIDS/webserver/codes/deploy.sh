#!/bin/bash  

user="ec2-user"
sourceDIR="/home/$user/cloudIDS/webserver/codes"
tomcatDIR="/usr/share/tomcat"

# Copy the latest files to tomcat/lib
cd $sourceDIR
sudo cp Config.txt $tomcatDIR/webapps/ROOT/cloudIDS
sudo cp lib/* $tomcatDIR/lib
sudo cp cloudIDSWeb.jar $tomcatDIR/lib

# Restart tomcat
sudo service tomcat restart
