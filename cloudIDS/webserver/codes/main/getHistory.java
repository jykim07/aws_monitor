package main;

import java.util.*;
import java.lang.*;
import java.text.SimpleDateFormat;
import com.mongodb.*;
import java.net.UnknownHostException;

public class getHistory
{
	static DBCollection collection;			
	
	static	ArrayList<String> timeType = new ArrayList<String>();
	static	ArrayList<String> metrics = new ArrayList<String>();
	
	static String data = "";
	
	public getHistory(DB db, String collName) throws MongoException 
	{
		try 
		{
			collection = db.getCollection(collName);
			timeType.clear();
			timeType.add("min");
		} 
		catch(MongoException e) {
			System.out.println(e.getMessage());
				e.printStackTrace();
		}	
	}
	
	public getHistory() {}
	
	public static String getResult(String clickedIP, String dataType)
	{
		//##### Set metrics to show####//
		metrics.clear();
		
		if(dataType.equals("metric"))
		{
			metrics.add("avg");
			metrics.add("min");
			metrics.add("max");
		}
		System.out.println("clickedIP : " + clickedIP + " type : " + dataType);
		//#############################//		
		Cursor cursor = getCursor(clickedIP);
				
		while(cursor.hasNext())
		{
			HashSet<dataFormatSystem> tsData = new HashSet<dataFormatSystem>();			
			BasicDBObject result = (BasicDBObject) cursor.next();	
			dataFormatSystem dataForm;
			
			for(int i=0; timeType.size() > i; i++)
			{
				List<BasicDBObject> object = (ArrayList<BasicDBObject>)result.get(timeType.get(i));

				if(object == null)
					continue;

				
				for(int j=0; object.size() > j; j++)
				{									
					for(int k=0; metrics.size() > k; k++)
					{
						String name = (String)object.get(j).get("name");
						String type = metrics.get(k);
						String value = (String)object.get(j).get(type);
						
						if(value != null)
						{			
							String strTS = (String)object.get(j).get("TS");
							int ts = Integer.parseInt(strTS);
							
							if(ts > (int)(System.currentTimeMillis()/1000))
								continue;

							dataForm = new dataFormatSystem(ts, name, value, type);	
							tsData.add(dataForm);							
						}
					}
				}
			}
											
			data = "[";
				
			Iterator<dataFormatSystem> it = tsData.iterator();
			
			while(it.hasNext())
			{
				dataForm = it.next();	

				data += "{\"Type\" : \"" + dataForm.getType() + "\",";
				data += "\"Name\" : \"" + dataForm.getAddr() + "\",";
				data += "\"TS\" : \"" + dataForm.getTS() + "\",";
				data += "\"Value\" : \"" + dataForm.getValue() + "\"}";	
		
				if(it.hasNext())
					data += ",";						
			}			

			if(cursor.hasNext())
				data += ",";
		}
			
		data += "]";
		
		System.out.println("DATA : " + data);		
		return data;
	}
	
	public static Cursor getCursor(String ip) throws MongoException
	{		
		ArrayList<String> monitorGroup = new ArrayList<String>();
		
		monitorGroup.add(ip);
		
//db.data.aggregate({$project:{"IP":"$IP","min":"$MIN.RQ.neighbor"}},{$match:{'IP':{$in:["10.1.128.24"]}}})
		BasicDBObject query = new BasicDBObject();		
		query.put("IP","$IP");
		query.put("exIP","$IP");
		query.put("min","$MIN.Metric.details");

		BasicDBObject project = new BasicDBObject("$project",query);	

		BasicDBObject match = new BasicDBObject("$match",new BasicDBObject("IP", new BasicDBObject("$in", monitorGroup)));
				
		List<BasicDBObject> pipeline = new ArrayList<BasicDBObject>();

		pipeline.add(project);	
		pipeline.add(match);				
			
		AggregationOptions aggregationOptions = AggregationOptions.builder()
			.batchSize(100)
			.outputMode(AggregationOptions.OutputMode.CURSOR)
			.build();
		
		Cursor cursor = collection.aggregate(pipeline,aggregationOptions);	
			
		return cursor;
	}
	
	static String getDate(int time)
	{
		long milliseconds = time * 1000L;
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		Calendar cal = Calendar.getInstance();
       cal.setTimeInMillis(milliseconds);
	   
	   return sdf.format(cal.getTime());
	}
}

