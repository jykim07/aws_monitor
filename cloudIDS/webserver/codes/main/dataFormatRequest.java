package main;

import java.util.*;
import java.lang.*;

public class dataFormatRequest
{
	int TS;
	String addr; // For getMetric : It should be a name of metric
	String port;
	String value;
	String type;
	
	public dataFormatRequest(int TS, String addr, String port, String value, String type)
	{
		this.TS = TS;
		this.addr = addr;
		this.port = port;
		this.value = value;
		this.type = type;
	}
	
	public int getTS()
	{
		return TS;
	}
	
	public String getPort()
	{
		return port;
	}	
	
	public String getAddr()
	{
		return addr;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public String getType()
	{
		return type;
	}
}