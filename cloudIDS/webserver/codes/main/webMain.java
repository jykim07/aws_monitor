/* Main class */

package main;

import configuration.*;
import servlet.*;

import java.util.*;
import java.lang.*;
import com.mongodb.*;
import java.net.UnknownHostException;

public class webMain
{
// Set physical information about mongocollection
	static String dbIP = null; // IP address
	static int dbPort; // Port number
	static String dbName = null; // 
	static String graphColl = null; // 
	static String historyColl = null; // 
	static String metricColl = null; // 	

//	public static void main(String args[])
	public webMain() throws UnknownHostException
	{
		Configuration.setConfiguration();
		dbIP = Configuration.getDBIP();
		dbPort = Configuration.getDBPort();
		dbName = Configuration.getDBName();
		graphColl = Configuration.getGraphColl();
		historyColl = Configuration.getHistoryColl();
		metricColl = Configuration.getMetricColl();
		
		DBconnection db = new DBconnection(dbIP, dbPort, dbName);
		System.out.println("## Got DB");
		ActionServlet servlet = new ActionServlet();
		getGraph graph = new getGraph(db.getThisDB(), graphColl);
		getMetric metric = new getMetric(db.getThisDB(), metricColl);		
		getHistory history = new getHistory(db.getThisDB(), historyColl);	
//		getUtilization util = new getUtilization(db.getThisDB(), historyColl);	
//		getRequests RQ = new getRequests(db.getThisDB(), historyColl);	
//		getUA ua = new getUA(db.getThisDB(), historyColl);
//		getAllUAs allUAs = new getAllUAs(db.getThisDB(), historyColl);

//		System.out.println(metric.getResult("172.31.2.109", "metric"));
//		System.out.println(util.getResult("10.1.1.2", "system"));
//		System.out.println(graph.getResult());
//		System.out.println(metric.getResult("172.31.2.109", "metric"));
	}
}
