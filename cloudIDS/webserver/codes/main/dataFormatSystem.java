package main;

import java.util.*;
import java.lang.*;

public class dataFormatSystem
{
	int TS;
	String addr; // For getMetric : It should be a name of metric
	String value;
	String type;
	
	public dataFormatSystem(int TS, String addr, String value, String type)
	{
		this.TS = TS;
		this.addr = addr;
		this.value = value;
		this.type = type;
	}
	
	public int getTS()
	{
		return TS;
	}
		
	
	public String getAddr()
	{
		return addr;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public String getType()
	{
		return type;
	}
}